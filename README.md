# Dieting Dapp

Deze app is een leerproject gericht op het kennismaken met web3.js en het gebruik van Solidity. 
Het idee om een fitness-dapp te maken kwam voort uit een test gericht op het lezen en schrijven van waardes naar een smart-contract.
Verder diende dit project als een eerste stap in het leren gebruiken van de web3.js library en de Solidity programmeer taal.

Het project bestaat uit een website waar de gebruiker een web3 wallet kan koppelen en vervolgens fitness-doelen kan stellen door een kleine hoeveelheid Ethereum te betalen.
Wanneer de gebruiker dit doel behaald heeft krijgt hij/zij een groter bedrag uitbetaald.
Verder kan men zijn/haar informatie koppelen aan een Web3 wallet. Zodat hun doelen en huidige gewicht altijd opgehaald kunnen worden wanneer ze opnieuw inlogen.

![User Interface](interface.png)

---

## Libraries en Development keuzes

In het ontwikkelen van deze app heb ik onderzoek gedaan naar verschillende libraries en technieken.

### Web3 Wallet

een Web3 wallet is net als een standaard crypto-wallet een plek om de "Secret key" van de Ethereum adres te bewaren en transacties aan te maken.
Web3 wil zeggen dat de wallet gekoppeld kan worden met een webapplicatie om transacties naar smart-contracten te sturen en eventueel crypto te ontvangen. 

![metamask](metamask.webp)


### Web3 Library
De term "Web3 library" refereert naar de library of module die het mogelijk maakt om vanuit het frontend te communiceren met de blockchain.
Om dit te doen zijn er verschillende libraries beschikbaar. De populairste twee zijn `web3.js` en `ethers.js`. De `*.js` geeft al een indicatie dat deze libraries geschreven en bedoelt zijn voor gebruik in javascript applicaties.
Er zijn ook libraries beschikbaar voor andere talen. Omdat Javascript in vrijwel iedere web-applicatie gebruikt wordt zijn deze twee het populairst.

Omdat ik de documentatie van `web3.js` het beste vond heb ik besloten om deze library te gebruiken voor mijn applicatie.

### Solidity
Om het Smart-contract van mijn app te schrijven heb ik gebruik gemaakt van Solidity. Dit is de taal ontwikkeld door de *Ethereum Foundation*. Deze taal kan worden gecompiled naar machine-code voor de "Ethereum Virtual Machine" (EVM). Deze EVM draait naast de standaard blockchain en voert dus de backend acties uit van het smart-contract.

Als Programmeer-taal is Solidity te vergelijken met moderne "Strong typed" programmeertalen zoals Rust of Go. Met een aantal syntax elementen die lijken op Python.
Verder was het proces van compilen en deployen naar een Blockchain wel nieuw. Dit maakt het testen van smart-contracten lastig omdat dit veel tijd in beslag kan nemen.

![Solidity](smart-contract.png)

---

## Functies

Het bijhouden van je gewicht op een smart-contract

het stellen/behalen/verwijderen van doelen die ethereum uitbetalen aan de gebruiker wanneer ze behaald worden.

## opzetten project

1. start je locale Ganache blockchain
2. navigeer naar de `/smart-contracts` folder.
3. deploy de smart-contracts met `truffle migrate`.
4. kopieer het contract address van het command output naar de `contractAddress` variabele in `../web/public/index.js`. (deze staat helemaal bovenaan het bestand)
5. navigeer naar de `/web` folder en installeer de benodigde node modules met `npm i`. (node 16.13.1, ookal zal enige recente lts versie het ook doen).
6. start de lokale server met `node server.js`.
7. De app is nu te gebruiken op `http://localhost:3000`.

### Developer
Mathijs van Kemenade
