//SPDX-License-Identifier: GPL
pragma solidity ^0.8.5;

contract dietApp {

    //struct for single goal
    struct userGoal {
        uint target;
        uint reward;
        bool completed;
    }

    //struct for the users
    struct user {
        bool isActive;
        string name;
        uint256 currentWeight;
        uint completedGoals;

    }

    //user and goals mapping
    mapping(address => user) userBase;
    mapping(address => userGoal[]) userGoals;

    constructor() {
    }

    //add new user with basis properties
    function registerUser(string memory name, uint currentWeight) public payable returns(uint) {
        userBase[msg.sender] = user(true, name, currentWeight, 0);

        return(200);
    }

    //returns the user or an empty user is no user is registered
    function getUser() public view returns (user memory) {
        if(!userBase[msg.sender].isActive) return user(false, 'null', 0, 0);
        return(userBase[msg.sender]);
    }

    //updates a users weight property
    function setWeight(uint weight) public payable returns (uint) {
        require(userBase[msg.sender].isActive, "This user does not exist.");
        userBase[msg.sender].currentWeight = weight;

        return (200);
    }

    //adds a new goal to the users goal array
    function setGoal(uint target) public payable returns(userGoal memory) {
        require(userBase[msg.sender].isActive, "This user does not exist.");
        require(msg.value >= 0, "No transaction value.");

        userGoals[msg.sender].push(userGoal(target, msg.value, false));
        uint length = userGoals[msg.sender].length;

        return(userGoals[msg.sender][length - 1]);
    }

    //removes a goal by shifting it to the end of the array and calling pop()
    function deleteGoal(uint index) public payable returns(uint) {
        require(userBase[msg.sender].isActive, "This user does not exist.");
        if (index >= userGoals[msg.sender].length) return(400);

        for (uint i = index; i < userGoals[msg.sender].length-1; i++) {
            userGoals[msg.sender][i] = userGoals[msg.sender][i+1];
        }
        userGoals[msg.sender].pop();

        return(200);
    }

    //returns all user goals
    function getGoals() public view returns (userGoal[] memory) {
        require(userBase[msg.sender].isActive, "This user does not exist.");

        return(userGoals[msg.sender]);
    }

    //loops through all goals and tests if the users weight value is equal or lower that the goal. If it is it adds all the rewards together and sends it to the user.
    function checkGoals() public payable returns(uint) {
        require(userBase[msg.sender].isActive, "This user does not exist.");
        
        uint totalRewards = 0;

        for(uint i = 0; i < userGoals[msg.sender].length; i++) {
            if (userBase[msg.sender].currentWeight <= userGoals[msg.sender][i].target) {
                totalRewards = totalRewards + userGoals[msg.sender][i].reward;
                userGoals[msg.sender][i].completed = true;
            }
        }
        //unused variable cannot be removed but I don't need it :(
        (bool sent, bytes memory data) = msg.sender.call{value: totalRewards}("");

        if(sent){
            return(200);
        } else {
            return(400);
        }

    }

}