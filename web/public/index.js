const contractAddr = '0x0bFf40b8EEf7dfeCc8ab8C73B67CE1dDBb0210fA';

main();

async function main() {

    //instantiate web3 and connect metamask
    const isConnected = await connectWeb3();
    window.defaultAddress = await ethereum.request({ method: 'eth_accounts' });
    //instaniate and set contract
    window.contract = new web3.eth.Contract(abi, contractAddr);
    contract.handleRevert = true;

    //manual reconnect if user denies connection prompt
    document.querySelector('.connect').addEventListener('click', function(){
        if(!isConnected) {
            connectWeb3();
        }
    });

    //user registration
    document.querySelector('.registerUser').addEventListener('click', async () => {
        let fullName = document.querySelector('.fullName').value;
        let weight = document.querySelector('.weight').value;
        await createProfile(contract, defaultAddress[0], fullName, weight);
    });
    //adds a new goal
    document.querySelector('.submitGoal').addEventListener('click', async () => {
        let weight = document.querySelector('.targetWeight').value;
        let reward = document.querySelector('.reward').value;
        await setGoal(contract, defaultAddress[0], weight, reward);
    });
    //modifies the users weight
    document.querySelector('.submitWeight').addEventListener('click', async () => {
        let weight = document.querySelector('.newWeight').value;
        await setWeight(contract, defaultAddress[0], weight);
    });
    //checks if the user completed any goals
    document.querySelector('.checkGoals').addEventListener('click', async () => {
        await getRewards(contract, defaultAddress[0]);
    })

    //starts the functions needed for getting user data or showing register function
    getData();

}

//gets user goals if the user has an account. Otherwise shows register prompt.
async function getData() {
    //get user data
    let userData = await getProfile(contract, defaultAddress[0]);
    console.log(userData);
    //if user exists get other info, otherwise allow registration
    if(!userData.isActive){
        alert("you dont have an account. Please register on the right side");
        toggleRegister();
    } else {
        //get all user goals
        let goals = await getGoals(contract, defaultAddress[0]);
        console.log(goals);
    }
}

//toggles the register form
function toggleRegister() {
    let register = document.querySelector('.register');
    let profile = document.querySelector('.profile');

    if(register.style.display == 'none') {
        register.style.display = 'block';
        profile.style.display = 'none';
    } else {
        register.style.display = 'none';
        profile.style.display = 'block';
    }
}


// inits the web3.js and metamask connection
async function connectWeb3() {
    if (window.ethereum) {
        window.web3 = new Web3(window.ethereum);
        await window.ethereum.request({method: 'eth_requestAccounts'});
        await ethereum.request({ method: 'eth_accounts' }).then((accounts) => {
            document.querySelector('.eth-address').innerHTML = accounts[0];
            document.querySelector('.connect').innerHTML = "Disconnect";
        });
        return true;
      }
    return false
}
//gets the profile and writes that data to the appropriate fields
async function getProfile(contract, address) {
    return await contract.methods.getUser().call({from: address}, (error, result) => {
        if(!error) {
            document.querySelector(".profile-info").innerHTML = "<li class='info-item'>Your Name: " + result.name + "</li><br>";
            document.querySelector(".profile-stats").innerHTML = "<li class='info-item'>Current Weight: " + result.currentWeight + "</li><br><li class='info-item'>Completed Goals: " + result.completedGoals + "</li><br>";

            return result;
        } else {
            throw error;
        }
    });
}

// creates profile and syncs other data fields afterwards
async function createProfile(contract, address, name, weight) {
    return await contract.methods.registerUser(name, weight).send({from: address}).then(function(receipt){
        console.log(receipt);
        toggleRegister();
        getData();
    });
}

//updates the users weight attribute
async function setWeight(contract, address, weight) {
    return await contract.methods.setWeight(weight).send({from: address}).then(function(receipt){
        console.log(receipt);
    });
}

//retrieves goals and builds list with interactive buttons.
async function getGoals(contract, address) {
    return await contract.methods.getGoals().call({from: address}, (error, result) => {
        if(!error) {
            document.querySelector(".goals-list").innerHTML = '';
            for( var i = 0; i < result.length; i++) {

                var removeButton = document.createElement('button');
                removeButton.type = "button";
                removeButton.className = "remove-button";
                removeButton.style.float = "right";
                removeButton.innerHTML = "remove goal";
                removeButton.dataset.index = i;

                var listElement = document.createElement('li');
                if(result[i].completed) {
                    listElement.className = "list-item completed";
                } else {
                    listElement.className = "list-item";
                }
                
                listElement.innerHTML = "<p>#" + i + " Target weight: " + result[i].target + " kg," + " Reward: " + result[i].reward / 1000000000000000000+ " ETH</p>"
                listElement.appendChild(removeButton);

                document.querySelector(".goals-list").appendChild(listElement);     
                
            };

            let buttons = document.getElementsByClassName('remove-button');

            for( var i = 0; i < buttons.length; i++) {
                buttons[i].addEventListener('click', async (event) => {
                    console.log(event.target.dataset.index);
                    await deleteGoal(contract, address, event.target.dataset.index);
                });
            }
            
            await getProfile();
            
            return result;

        } else {
            console.log(error);
        }
    });
}
//creates a new goal and retrieves it afterwards
async function setGoal(contract, address, targetWeight, reward) {
    return await contract.methods.setGoal(targetWeight).send({from: address, value: reward * 1000000000000000000}).then(function(receipt){
        console.log(receipt);
        console.log( getGoals(contract, address));
    });
}
//removes a goal based on its index
async function deleteGoal(contract, address, index) {
    return await contract.methods.deleteGoal(index).send({from: address}).then(function(receipt){
        console.log(receipt);
        getGoals(contract, address)
    });
}
//triggers the payout function
async function getRewards(contract, address) {
    return await contract.methods.checkGoals().send({from: address}).then(function(receipt){
        console.log(receipt);
        getGoals(contract, address);
    });
}